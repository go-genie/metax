module gitee.com/go-genie/metax

go 1.21

require (
	gitee.com/go-genie/xx v1.0.1
	github.com/onsi/gomega v1.18.1
)

require (
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
